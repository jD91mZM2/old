# THIS IS BLOAT

Use [inc](https://gitlab.com/jD91mZM2/inc) instead

# some good old scaffold

Scaffolding projects is boring. You want to create a `.gitlab-ci.yml`? Welcome
to copy-pasting different yml configs together into a new abomination. Want to
create a `.pre-commit-config.yaml`? Same story. License? Copy paste, update
your name, update the year, ugh.

## old

old is a simple project that cuts through the crap. You select which configs you want to merge:

``` sh
old nix rust
```

...and `old` merges all template configs into one.

## Example pls?

The `example-repo/` directory is an example repository. It includes an
`old.yml` which configures old, but you can use many configuration formats -
JSON and YAML are currently the ones supported.
