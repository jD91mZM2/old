from dataclasses import dataclass
from pathlib import Path
from typing import Callable
import json
import yaml

from .merge import merge
from . import text


@dataclass
class ConfigType:
    reader: Callable[[str], dict]
    writer: Callable[[dict], str]


class ConfigTypes:
    JSON = ConfigType(json.loads, json.dumps)
    YAML = ConfigType(yaml.safe_load, yaml.dump)
    TEXT = ConfigType(text.loads, text.dumps)


TYPE_FROM_EXT = {
    ".json": ConfigTypes.JSON,
    ".yaml": ConfigTypes.YAML,
    ".yml": ConfigTypes.YAML,
    ".txt": ConfigTypes.TEXT,
    "": ConfigTypes.TEXT,
}


class Config(dict):
    def __init__(self, config):
        self.update(config)

    def read(filename):
        """
        Load a config file from filename
        """
        filename = Path(filename)
        reader = TYPE_FROM_EXT[filename.suffix].reader

        text = filename.read_text()
        return Config(reader(text))

    def read_any(filename):
        """
        Load a config file with unknown extension
        """

        for ext in TYPE_FROM_EXT.keys():
            file = filename.with_suffix(ext)
            if file.exists():
                return Config.read(file)

        raise Exception("No config file found in repo directory")

    def write(self, filename):
        """
        Write a config file
        """
        filename = Path(filename)
        writer = TYPE_FROM_EXT[filename.suffix].writer

        text = writer(dict(self))
        filename.write_text(text)

    def merge(self, other):
        """
        Merge two config files
        """
        return Config(merge(self, other))
