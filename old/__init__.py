from .config import Config
from pathlib import Path
import argparse
import os


def main():
    parser = argparse.ArgumentParser(description="Scaffold your projects")
    parser.add_argument("templates", nargs="*", type=str)

    args = parser.parse_args()

    if "OLD_REPO_DIRS" not in os.environ:
        print("No $OLD_REPO_DIRS variable found.")
        print("You need to set it up as a '" + os.pathsep + "'-separated list")
        print("of directories that contain an old.yml / old.json / etc")
        print("config file.")
        return

    repo_dirs = list(map(Path, os.environ["OLD_REPO_DIRS"].split(os.pathsep)))

    final_config = Config({})

    # Load all repositories
    for repo_dir in repo_dirs:
        config = Config.read_any(repo_dir.joinpath("old"))

        for template in config["templates"].values():
            for file in template["files"]:
                file["config"] = Config.read(repo_dir.joinpath(file["input"]))

        final_config = final_config.merge(config)

    # List all templates if none specified
    if len(args.templates) == 0:
        print("Templates:")
        for template in sorted(final_config["templates"].keys()):
            print("- " + template)
        return

    # Create set of file => config
    files = Config({})

    for template in args.templates:
        if template not in final_config["templates"]:
            print("Template not found: " + template)
            return

        local_files = Config({})

        template = final_config["templates"][template]
        for file in template["files"]:
            output = Path(file["output"])
            local_files[output] = file["config"]

        files = files.merge(local_files)

    # Save all files
    for file, config in files.items():
        if file.exists():
            existing = Config.read(file)
            config = existing.merge(config)

        config.write(file)

        print("Wrote " + str(file))
