from copy import copy


def merge_attrs(first, second):
    """
    Merge dictionaries/hashmaps/attribute sets
    """
    final = copy(first)

    firstkeys = first.keys()
    secondkeys = second.keys()

    # Keys only in second
    for key in secondkeys - firstkeys:
        final[key] = second[key]

    # Keys in both
    for key in firstkeys & secondkeys:
        final[key] = merge(final[key], second[key])

    return final


def merge_list(first, second):
    """
    Merge lists
    """
    return first + second


def merge(first, second):
    """
    Merge any type
    """

    # If types are different, second takes precedence
    if not isinstance(first, type(second)) or not isinstance(second, type(first)):
        return second

    if isinstance(first, dict):
        return merge_attrs(first, second)
    elif isinstance(second, list):
        return merge_list(first, second)
    else:
        return second
