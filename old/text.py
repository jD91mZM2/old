import chevron
import subprocess


def execute(text, _render):
    process = subprocess.run(text, shell=True, capture_output=True, text=True)
    return process.stdout.strip()


def strip(text, render):
    return render(text).strip()


def loads(string):
    text = chevron.render(
        string,
        {
            "execute": execute,
            "strip": strip,
        },
    )
    return {
        "text": text,
    }


def dumps(obj):
    return obj["text"]
